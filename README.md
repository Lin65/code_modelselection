# Bayesian model selection framework for identifying growth patterns in filamentous fungi #

A Bayesian model selection framework is implemented and applied to comparing different fungal growth model. 

This code is for the paper "Bayesian model selection framework for identifying growth patterns in filamentous fungi"
http://www.sciencedirect.com/science/article/pii/S0022519316001624

# Dependency #

This code is written in matlab and c++. 

The code is dependent on the following package:

QUESO: http://libqueso.com