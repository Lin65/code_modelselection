%% ------------------------------------------------------------------------
% True system
%--------------------------------------------------------------------------


% true parameters
v = 0.8;
y1 = 0.03;
a1 = 0.08;
b2 = 0.3;

% true p
p_true = [ v y1 a1 b2];

%% ------------------------------------------------------------------------
% Initial uncertainty
%--------------------------------------------------------------------------

init_unc = 1;     % Gaussian
                  %init_unc = 1;     % Uniform

% uncertainty initial condition - normal distribution (mean, std)

if( init_unc == 0)
    % for Gaussian
        p_bounds = ...
        [ 0.5      0.2 ; ...     	% v
		  0.05     0.02 ; ...		% a1
          0.5      0.2 ];           % b2
else
    % for Uniform
       p_bounds = ...
        [ 0      1 ; ...     	% v
		  0      0.1 ; ...		% a1
          0.1      1 ; ...       % b2
		  100*eps  1 ; ...      % sigma2 for hyphal density
		  100*eps  1 ];         % sigma2 for tip density
end;

prior_setting.bounds = p_bounds;
prior_setting.HDalpha = 10;
prior_setting.HDbeta = 0.0003;

prior_setting.TDalpha = 10;
prior_setting.TDbeta = 0.0003;

%% ------------------------------------------------------------------------
% Model
%--------------------------------------------------------------------------

model.n = 5;		% dim of parameters:  v a1 b2 and variance of model discrepancy

% Default
model.m = 2;				    % dim meas model: Predator-Prey Model

model.smps = 5000;

n_obs = 10;
meas_n = zeros(n_obs, 1);

        
