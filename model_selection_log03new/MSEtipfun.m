function MSEval = MSEtipfun( p )
%MSEFUN Summary of this function goes here
%   Detailed explanation goes here
a = p(1);
b = p(2);
c = p(3);
MSEval = 0;
x_span_use =  [  2.5126    5.0251    7.5377   10.0503   12.5628   15.0754   17.5879   20.1005   ];
n_state_use = [  -3.1799   -1.1554   -0.7345   -1.8967   -4.6273   -8.8893  -14.6480  -21.8701  ];
for i = 1:8
    MSEval = MSEval + ((log(a) - b*(x_span_use(i) - c)^2) - n_state_use(i))^2;
end

MSEval = 1/8*MSEval;

