A = 10;
B = 15;
X = 0:0.01:1;
Y = B^A./gamma(A)*X.^(-A-1).*exp(-B./X);
figure;
plot(X,Y);