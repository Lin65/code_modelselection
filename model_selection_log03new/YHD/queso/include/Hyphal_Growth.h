#include <math.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <uqGslMatrix.h>

/*typedef struct
{
	double hyphalDensity[200];
  double tipDensity[200];
}density;*/
 

double* hyphal_growth_YHD(double paramValues[], int day)
{ 
	int Nx = 200;     //total # of steps for distance
	int Nt = 451;    //total # of steps for time
 // float mu = 6.5;   //for initial Gaussian curve
 // float sigma = 2;  //for initial Gaussian curve
  float dx;         //size of each step for distance
  float dt;         //size of each step for time
  double v = paramValues[0];
  double y1 = paramValues[1];
  double a1 = paramValues[2];
  double b2 = paramValues[3];
  int i;
  int j;
  float *x = new float[Nx];  
	float *t = new float[Nt];
  std::fill(x, x+Nx, 0);
	std::fill(t, t+Nt, 0);

  double **p=new double*[Nx];
  for(i=0; i<Nx; i++)
		p[i]=new double[Nt];
	//std::fill_n(p, Nx*Nt, 0);

  double **n=new double*[Nx];
  for(i=0; i<Nx; i++)
		n[i]=new double[Nt];
	//std::fill_n(n, Nx*Nt, 0);

	for(i =0; i<Nx; i++)
	{
    for(j = 0; j<Nt; j++)
      { 
        p[i][j]=0;
        n[i][j]=0;
      } 
	}

  //x = linspace(0,100,Nx)
  for(i=0; i<Nx; i++)
	{
		x[i] = i*100.0/(Nx - 1);		
	}

  //t = linspace(0,5,Nx)
  for(i=0; i<Nt; i++)
	{
		t[i] = i*4.5/(Nt - 1);		
	}
  dx = x[1] - x[0];
	dt = t[1] - t[0];

  //initial condition
	for(i=0; i<50; i++) 
	{
        p[i][0] = exp(-0.0351 *pow((x[i] - 1.0516), 2));
        n[i][0] = 0.4777*exp(-0.1223 *pow((x[i] -6.9372),2));
	}

  delete []t;
  delete []x;
  //boundary condition
  for(j=0; j<day; j++)
  {
    p[0][j] = 1;
  }
  //recursion function
  for(j=0; j<day-1; j++)
  {
    for(i=1; i<Nx-1; i++)
    {
      p[i][j+1] = dt*v*n[i][j] + p[i][j] - dt*y1*p[i][j];
      n[i][j+1] = -v*dt/(2*dx)*n[i+1][j] + v*dt/(2*dx)*n[i-1][j] + (1+ a1*dt)*n[i][j] - b2*dt*n[i][j]*p[i][j];
    }
  }   

	double* result = new double[20];
  
  for(i=0; i<10; i++)
  {
    result[i] = p[i*5][day-1];        
  } 
    for(i=0; i<10; i++)
  {
    result[i+10] = n[i*5][day-1];        
  } 

  for(i=0;i<Nx;i++)
  {
    delete []p[i];
    p[i]=NULL;
  }
  delete []p;
  p=NULL;

  for(i=0;i<Nx;i++)
  {
    delete []n[i];
    n[i]=NULL;
  }
  delete []n;
  n=NULL;

  return result;
  
}





