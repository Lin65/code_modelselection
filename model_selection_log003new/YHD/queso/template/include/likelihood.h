/*--------------------------------------------------------------------------
 *--------------------------------------------------------------------------
 *
 * Copyright (C) 2008 The PECOS Development Team
 *
 * Please see http://pecos.ices.utexas.edu for more information.
 *
 * This file is part of the QUESO Library (Quantification of Uncertainty
 * for Estimation, Simulation and Optimization).
 *
 * QUESO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QUESO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QUESO. If not, see <http://www.gnu.org/licenses/>.
 *
 *--------------------------------------------------------------------------
 *
 * $Id: exStatisticalInverseProblem1_likelihood.h 6059 2009-10-28 12:51:54Z karl $
 *
 * Brief description of this file: 
 * 
 *--------------------------------------------------------------------------
 *-------------------------------------------------------------------------- */

#ifndef __LIKELIHOOD_H__
#define __LIKELIHOOD_H__

//#include <uqGslMatrix.h>
//#include <math.h>  
//#include <iostream>

#include "Hyphal_Growth.h"

//********************************************************
// The likelihood routine: provided by user and called by QUESO
//********************************************************
template<class P_V,class P_M>
struct
likelihoodRoutine_DataType
{
  std::vector<double> m_StateData;
  std::vector<double> m_ObsData;
  std::vector<double> m_Var;
  P_V* m_likeliVector;
  P_V* m_IntermVector;
  P_M* m_likelicovMatrix;
};

template<class P_V,class P_M>
double
likelihoodRoutine(
  const P_V&  paramValues,
  const P_V*  paramDirection,
  const void* functionDataPtr,
  P_V*        gradVector,
  P_M*        hessianMatrix,
  P_V*        hessianEffect)
{
  double result = 0.;

  std::vector<double>& StateData      = ((likelihoodRoutine_DataType<P_V,P_M> *) functionDataPtr)->m_StateData;
  std::vector<double>& ObsData        = ((likelihoodRoutine_DataType<P_V,P_M> *) functionDataPtr)->m_ObsData;
  std::vector<double>& Var            = ((likelihoodRoutine_DataType<P_V,P_M> *) functionDataPtr)->m_Var;
  P_V& likeliVector                   = *((likelihoodRoutine_DataType<P_V,P_M> *) functionDataPtr)->m_likeliVector;
  P_V& IntermVector                   = *((likelihoodRoutine_DataType<P_V,P_M> *) functionDataPtr)->m_IntermVector;
  P_M& likelicovMatrix                = *((likelihoodRoutine_DataType<P_V,P_M> *) functionDataPtr)->m_likelicovMatrix;

  double ExValue = 0.;

  const double PI  =3.14159;

//  for (unsigned int i = 0.; i < StateData.size(); i++) {
//      ExValue = ExValue + (paramValues[0] + paramValues[1]*StateData[i] - ObsData[i])*1/(pow(Var[i],2) + pow(paramValues[2],2))*(paramValues[0] + paramValues[1]*StateData[i]  - ObsData[i]);
// }

//result = log(1/(sqrt(2*PI*(pow(Var[0],2) + pow(paramValues[2],2))))) - 0.5*ExValue;
//********************************************************
// uqVectorSpaceClass<P_V,P_M> likelihoodSpace(env,"likeli_",6,NULL);
 //P_M likelicovMatrix( likelihoodSpace.zeroVector() ); 
std::cout<<"4.so far OK\n"<<StateData.size()<<StateData.size();
 for (int i = 0; i<StateData.size(); i++)
    {
     for (int j = 0; j<StateData.size(); j++)
         {
          if (i==j&&i<10)
             {
              likelicovMatrix(i,j) = paramValues[4];
             }
          if (i==j&&i>9)
             {
              likelicovMatrix(i,j) = paramValues[5];
             }
          std::cout << likelicovMatrix(i,j) << "";
          if (j == StateData.size() - 1)
             {
              std::cout <<"\n";
             }
         }
    }
std::cout<<"5.so far OK\n";
 //P_V likeliVector( likelihoodSpace.zeroVector() );
 //P_V IntermVector( likelihoodSpace.zeroVector() );

 double *output;
 double *input = new double[4];

 input[0] = paramValues[0];
 input[1] = paramValues[1];
 input[2] = paramValues[2];
 input[3] = paramValues[3];

 output = hyphal_growth_YHD(input, 451);
 
 for (int i = 0; i<StateData.size(); i++)
    { 
      if (output[i] < 1e-10){output[i] = 1e-10;}
         
      likeliVector[i] = log(output[i]) - ObsData[i];
    }
 delete []output;
 delete []input;

 likelicovMatrix.invertMultiply(likeliVector, IntermVector);

 for (int i = 0; i<StateData.size(); i++)
     {
      ExValue = ExValue + likeliVector[i]*IntermVector[i];
     } 
 result = -0.5*log(likelicovMatrix.determinant()) - 0.5*ExValue;

 
//********************************************************

  return result;

}
#endif // __EX_STATISTICAL_INVERSE_PROBLEM_LIKELIHOOD_H__
