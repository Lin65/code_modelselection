clear all;
close all;
clc;

% settings GP
sigma_GP = 1;
covlen_GP = 1;
meas_err = 0.001;

% number of points 
no_pts = 100;
x_dom = linspace(0, 5, no_pts);

% covariance function & sample function
kK = kernVecOrMat_SE( x_dom, x_dom, sigma_GP, covlen_GP, meas_err );
my_func = chol(kK)'*randn(no_pts,1);

% plot the sample from the Gaussian process
figure; hold on;

func_lb = zeros(1, no_pts) - 3*diag(kK)';
func_ub = zeros(1, no_pts) + 3*diag(kK)';

plot( x_dom, func_lb, 'r', 'Linewidth', 2 );
plot( x_dom, func_ub, 'r', 'Linewidth', 2 );
plot( x_dom, my_func, 'b' );
drawnow;



