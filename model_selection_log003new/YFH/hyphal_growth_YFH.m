function [X,P,N] = hyphal_growth_YFH(params, day)

% create the mesh
Nx = 200;
Nt = 451;

x = linspace(0,100,Nx);
t = linspace(0.5,5,Nt);

dx = x(2)-x(1);
dt = t(2)-t(1);

% matrices to store the solution
p = zeros(Nx,Nt);
n = zeros(Nx,Nt);

v = params(1);
a2 = params(2);
a1 = params(3);
b2 = params(4);


% initial condition
for i =1:50
    p(i,1) = exp(-0.0357*(x(i) - 1.3116)^2);%exp(-0.01*i^2);
    n(i,1) = 0.4761*exp(-0.1223*(x(i) - 6.9407)^2);%exp(-0.01*i^2);
end;

% boundary condition
for j = 1:day
   p(1, j) = 1;    % left boundary
   n(1, j) = 0;    % left boundary
end;

for j = 1:(day-1)
   for i = 2:(Nx-1)      % how to fix it??
       p(i, j+1) = dt*v*n(i, j) + p(i, j) ;%- dt*y1*p(i,j);
	 %  n(i, j+1) = -dt/dx*n(i+1, j) + (1+ dt + dt/dx)*n(i, j) - dt*n(i, j)*p(i, j); % forward difference
	   n(i, j+1) = -v*dt/(2*dx)*n(i+1, j) + v*dt/(2*dx)*n(i-1, j) + (1+ a1*dt)*n(i, j) + a2*dt*p(i, j) - b2*dt*n(i, j)*p(i, j); %central difference
%        if p(i, j+1)<0
%            p(i, j+1)=0;
%        end;
%        if n(i, j+1)<0
%            n(i,j+1)=0;
%        end;
   end;
end;

% generate the profile needed: 1 to calibrate, 2 to validate

P = p(:,1:day);
N = n(:,1:day);
X = x;







